package com.gbournac.question.common.domain.dto;

import java.util.Date;

import lombok.Data;

/**
 * The error message DTO
 *
 * @author Guénaël Bournac
 *
 */
@Data
public class ErrorMessageDTO {

	/**
	 * The error timestamp
	 */
	private Date timestamp;

	/**
	 * The error code to help debugging
	 */
	private String code;

	/**
	 * The error message to display it to users
	 */
	private String message;

	/**
	 * The constructor
	 *
	 * @param code
	 * @param message
	 */
	public ErrorMessageDTO(final String code, final String message) {
		this.timestamp = new Date();
		this.code = code;
		this.message = message;
	}
}
