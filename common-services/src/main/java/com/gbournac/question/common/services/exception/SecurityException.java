package com.gbournac.question.common.services.exception;

/**
 * Class for security exceptions
 *
 * @author Guénaël Bournac
 *
 */
public class SecurityException extends ProjectException {

	/**
	 *
	 */
	private static final long serialVersionUID = 4128020393007552724L;

	/**
	 * Constructor with the exception code
	 *
	 * @param code {@link ExceptionCode}
	 */
	public SecurityException(final ExceptionCode code) {
		super(code);
	}

	/**
	 * Constructor with error message and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 */
	public SecurityException(final ExceptionCode code, final String message) {
		super(code, message);
	}

	/**
	 * Constructor with error message, exception and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 * @param cause   {@link Throwable} exception
	 */
	public SecurityException(final ExceptionCode code, final String message, final Throwable cause) {
		super(code, message, cause);
	}

}
