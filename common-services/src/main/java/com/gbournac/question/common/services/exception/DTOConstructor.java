package com.gbournac.question.common.services.exception;

import com.gbournac.question.common.domain.dto.ErrorMessageDTO;

/**
 * Fabric class for the Error DTO
 *
 * @author Guénaël Bournac
 *
 */
public class DTOConstructor {

	/**
	 * private constructor
	 */
	private DTOConstructor() {

	}

	/**
	 * The fabric method for error dto
	 *
	 * @param exception {@link Project Exception}
	 * @return
	 */
	public static ErrorMessageDTO createDTO(final ProjectException exception) {
		return new ErrorMessageDTO(exception.getCode().getInternalProjectCode(), exception.getMessage());
	}
}
