package com.gbournac.question.common.services.constants;

/**
 * The attribute constants for the request
 *
 * @author Guénaël Bournac
 *
 */
public class AttributeConstants {

	/**
	 * Token to save the correlation UUID
	 */
	public static final String CORRELATION_HEADER = "X-CORRELATION";

	/**
	 * Token to save the start time attibute
	 */
	public static final String START_ATTRIBUTE = "START";
}
