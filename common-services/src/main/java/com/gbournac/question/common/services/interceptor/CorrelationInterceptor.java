package com.gbournac.question.common.services.interceptor;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.gbournac.question.common.services.constants.AttributeConstants;

@Component
public class CorrelationInterceptor {

	public void addCorrelationID(HttpServletRequest request) {
		if (request.getHeader(AttributeConstants.CORRELATION_HEADER) == null) {
			request.setAttribute(AttributeConstants.CORRELATION_HEADER, generateUUID());
		} else {
			request.setAttribute(AttributeConstants.CORRELATION_HEADER,
					request.getHeader(AttributeConstants.CORRELATION_HEADER));
		}
		Date date = new Date();
		request.setAttribute(AttributeConstants.START_ATTRIBUTE, date.getTime());
	}

	private String generateUUID() {
		return UUID.randomUUID().toString();
	}

}
