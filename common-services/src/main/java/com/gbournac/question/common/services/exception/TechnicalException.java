package com.gbournac.question.common.services.exception;

/**
 * Class for technical exceptions
 *
 * @author Guénaël Bournac
 *
 */
public class TechnicalException extends ProjectException {

	/**
	 *
	 */
	private static final long serialVersionUID = -1151167703631985353L;

	/**
	 * Constructor with the exception code
	 *
	 * @param code {@link ExceptionCode}
	 */
	public TechnicalException(final ExceptionCode code) {
		super(code);
	}

	/**
	 * Constructor with error message and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 */
	public TechnicalException(final ExceptionCode code, final String message) {
		super(code, message);
	}

	/**
	 * Constructor with error message, exception and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 * @param cause   {@link Throwable} exception
	 */
	public TechnicalException(final ExceptionCode code, final String message, final Throwable cause) {
		super(code, message, cause);
	}

}
