package com.gbournac.question.common.services.logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gbournac.question.common.services.constants.AttributeConstants;

/**
 * The log formater
 *
 * @author Guénaël Bournac
 *
 */
public class LogFormater {

	private static String NO_USER_ID = "0";

	/**
	 * Function to log the start function token
	 *
	 * @param logger
	 * @param sMethodName
	 * @param msg
	 * @param extraArgs
	 */
	public static void startFunction(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {
		if (logger.isDebugEnabled()) {
			logger.debug("[{}] - [{}] [START_FUNCTION] : " + msg, buildNewExtraArgs(sMethodName, extraArgs));
		}
	}

	/**
	 * Function to log the end function token
	 *
	 * @param logger
	 * @param sMethodName
	 * @param msg
	 * @param extraArgs
	 */
	public static void endFunction(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {
		if (logger.isDebugEnabled()) {
			logger.debug("[{}] - [{}] [END_FUNCTION] : " + msg, buildNewExtraArgs(sMethodName, extraArgs));
		}
	}

	/**
	 * Info log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void info(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {
		if (logger.isInfoEnabled()) {
			logger.info("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, extraArgs));
		}
	}

	/**
	 * Debug log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void debug(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {

		if (logger.isDebugEnabled()) {
			logger.debug("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, extraArgs));
		}
	}

	/**
	 * Warn log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void warn(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {

		logger.warn("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, extraArgs));
	}

	/**
	 * Error log method in case of no user connected.
	 *
	 * @param logger      Logger.
	 * @param sMethodName Method name.
	 * @param msg         Message.
	 * @param extraArgs   Extra arguments.
	 */
	public static void error(final Logger logger, final String sMethodName, final String msg,
			final Object... extraArgs) {

		logger.error("[{}] - [{}] : " + msg, buildNewExtraArgs(sMethodName, extraArgs));
	}

	/**
	 * Method to transform extra args.
	 *
	 * @param sMethodName Method name.
	 * @param userId      Connected user.
	 * @param extraArgs   Extra args
	 * @return List of extra args.
	 */
	private static Object[] buildNewExtraArgs(final String sMethodName, final Object... extraArgs) {

		final List<Object> argList = new ArrayList<>();

		final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes instanceof ServletRequestAttributes) {
			final HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
			argList.add(request.getAttribute(AttributeConstants.CORRELATION_HEADER));
		} else {
			argList.add(NO_USER_ID);
		}
		argList.add(sMethodName);
		argList.addAll(Arrays.asList(extraArgs));

		return argList.toArray();
	}

	public static void afterRequestLog(final Logger logger, final HttpServletRequest request,
			final HttpServletResponse response) {
		if (logger.isInfoEnabled()) {
			final long startDate = (Long) request.getAttribute(AttributeConstants.START_ATTRIBUTE);
			final Date endDate = new Date();
			final long time = endDate.getTime() - startDate;
			logger.info("[{}] - [END] - [{}] - [{}] - [{} {}] ",
					request.getAttribute(AttributeConstants.CORRELATION_HEADER), response.getStatus(), time,
					request.getMethod(), request.getRequestURI());
		}
	}

	public static void beforeRequestLog(final Logger logger, final HttpServletRequest request) {
		if (logger.isInfoEnabled()) {
			logger.info("[{}] - [START] - [{} {}] ", request.getAttribute(AttributeConstants.CORRELATION_HEADER),
					request.getMethod(), request.getRequestURI());
		}
	}

}
