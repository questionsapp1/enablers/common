package com.gbournac.question.common.services.exception;

import org.springframework.http.HttpStatus;

/**
 * The interface for the exception code to create an enum
 *
 * @author Guénaël Bournac
 *
 */
public interface ExceptionCode {

	/**
	 * Function to get the exception internal code
	 *
	 * @return
	 */
	String getInternalProjectCode();

	/**
	 * The http Status code to return
	 *
	 * @return
	 */
	HttpStatus getHTTPStatusCode();
}
