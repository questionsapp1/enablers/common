package com.gbournac.question.common.services.interceptor;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gbournac.question.common.services.logger.LogFormater;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Headers {

	public static void setHeader(final String key, final String value) {
		final String sMethodName = "setHeader";
		LogFormater.startFunction(log, sMethodName, "");

		final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		final HttpServletResponse response = ((ServletRequestAttributes) requestAttributes).getResponse();
		if (response != null) {
			response.setHeader(key, value);
		}

		LogFormater.endFunction(log, sMethodName, "");

	}

}
