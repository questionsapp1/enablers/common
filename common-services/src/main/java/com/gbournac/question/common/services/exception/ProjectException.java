package com.gbournac.question.common.services.exception;

import lombok.Getter;

/**
 * Class for the general exceptions
 *
 * @author Guénaël Bournac
 *
 */
@Getter
public class ProjectException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -5088847096695083194L;

	/**
	 * The code of the exception
	 */
	private final ExceptionCode code;

	/**
	 * Constructor with the exception code
	 *
	 * @param code {@link ExceptionCode}
	 */
	public ProjectException(final ExceptionCode code) {
		super();
		this.code = code;
	}

	/**
	 * Constructor with error message and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 */
	public ProjectException(final ExceptionCode code, final String message) {
		super(message);
		this.code = code;
	}

	/**
	 * Constructor with error message, exception and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 * @param cause   {@link Throwable} exception
	 */
	public ProjectException(final ExceptionCode code, final String message, final Throwable cause) {
		super(message, cause);
		this.code = code;
	}

}
