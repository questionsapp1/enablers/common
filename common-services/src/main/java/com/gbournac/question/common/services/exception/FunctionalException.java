package com.gbournac.question.common.services.exception;

/**
 * Functionnal exceptions class
 *
 * @author Guénaël Bournac
 *
 */
public class FunctionalException extends ProjectException {

	/**
	 *
	 */
	private static final long serialVersionUID = -5114333615983363244L;

	/**
	 * Constructor with the exception code
	 *
	 * @param code {@link ExceptionCode}
	 */
	public FunctionalException(final ExceptionCode code) {
		super(code);
	}

	/**
	 * Constructor with error message and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 */
	public FunctionalException(final ExceptionCode code, final String message) {
		super(code, message);
	}

	/**
	 * Constructor with error message, exception and the exception code
	 *
	 * @param code    {@link ExceptionCode}
	 * @param message {@link String} error message
	 * @param cause   {@link Throwable} exception
	 */
	public FunctionalException(final ExceptionCode code, final String message, final Throwable cause) {
		super(code, message, cause);
	}
}
